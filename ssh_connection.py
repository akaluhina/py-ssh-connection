import paramiko

HOST = "192.168.220.100"
USER = "admin"
PASSWORD = "P@ssw0rd"
PATTERN = "bank"

ssh_connection = paramiko.SSHClient()
ssh_connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())

ssh_connection.connect(HOST, username=USER, password=PASSWORD, look_for_keys=False)

stdin, stdout, stderr = ssh_connection.exec_command('find /')

for line in stdout:
    if PATTERN in line:
        print(line)

ssh_connection.close()

del stdin
